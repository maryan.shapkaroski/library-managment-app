from tkinter import *
from tkinter import ttk
from tkinter import messagebox
import sqlite3

con = sqlite3.connect('library.db')
cur = con.cursor()

class GiveBook(Toplevel):
    def __init__(self):
        Toplevel.__init__(self)
        self.geometry('650x750+550+200')
        self.title('Borrowed Books')
        self.resizable(False, False)

        query = "SELECT * FROM books WHERE book_status = 0"
        books = cur.execute(query).fetchall()
        book_list = []
        for book in books:
            book_list.append(str(book[0])+ ' - ' + book[1])

        query2 = "SELECT * FROM member"
        members = cur.execute(query2).fetchall()
        member_list = []
        for member in members:
            member_list.append(str(member[0])+ ' - ' + member[1])

        ### Frames ###

        # Top Frame
        self.topFrame = Frame(self,
                            height = 150,
                            bg = '#212121')
        self.topFrame.pack(fill = X)

        # Bottom Frame
        self.bottomFrame = Frame(self,
                                height = 600,
                                bg = '#252526')
        self.bottomFrame.pack(fill = X)

        # Heading, image 
        self.top_image = PhotoImage(file = 'img/addperson.png')
        self.top_image_lbl = Label(self.topFrame,
                                image = self.top_image,
                                bg = '#252526')
        self.top_image_lbl.place(x = 120, y = 10)
        heading = Label(self.topFrame,
                        text = 'Lend Book',
                        font = 'Helvetica',
                        fg = '#306998',
                        bg = '#212121')
        heading.place(x = 290, y = 60)


        ### Borrowed Window - Entries and labels ###

        # Borrowed Book
        self.book_name = StringVar()
        self.lbl_name = Label(self.bottomFrame,
                            text = 'Book Name ',
                            font = 'Helvetica',
                            fg = 'white',
                            bg = '#252526')
        self.lbl_name.place(x = 40, y = 40)
        self.combo_name = ttk.Combobox(self.bottomFrame,
                                    textvariable = self.book_name)
        self.combo_name['values'] = book_list
        self.combo_name.place(x = 150, y = 45)


        # Member that borrows - Name
        self.member_name = StringVar()
        self.lbl_surname = Label(self.bottomFrame,
                            text = 'Member Name : ',
                            font = 'Helvetica',
                            fg = 'white',
                            bg = '#252526')
        self.lbl_surname.place(x = 40, y = 80)
        self.combo_member = ttk.Combobox(self.bottomFrame,
                                    textvariable = self.member_name)
        self.combo_member['values'] = member_list
        self.combo_member.place(x = 150, y = 85)

        # Button
        btn = Button(self.bottomFrame,
                    text = 'Lend Book',
                    command = self.lendBook)
        btn.place(x = 220, y = 140)

    def lendBook(self):
        book_name = self.book_name.get()
        self.book_id = book_name.split(' - ')[0]
        member_name = self.member_name.get()

        if(book_name and member_name != ''):
            try:
                query = "INSERT INTO 'borrows' (bbook_id, bmember_id) VALUES (?, ?)"
                cur.execute(query,(book_name, member_name))
                con.commit()
                messagebox.showinfo("Success", "Successfully added to the database!", icon = 'info')
                cur.execute("UPDATE books SET book_status = ? WHERE book_id = ?", (1, self.book_id))
                con.commit()
            except:
                messagebox.showerror('Error', 'Can NOT be added to the database!', icon = 'warning')  
        else:
            messagebox.showerror('Error', 'Fields can NOT be empty!', icon = 'warning')        

