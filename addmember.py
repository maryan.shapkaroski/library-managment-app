from tkinter import *
from tkinter import messagebox
import sqlite3

con = sqlite3.connect('library.db')
cur = con.cursor()

class AddMember(Toplevel):
    def __init__(self):
        Toplevel.__init__(self)
        self.geometry('650x750+550+200')
        self.title('Add Member')
        self.resizable(False, False)

        ### Frames ###

        # Top Frame
        self.topFrame = Frame(self,
                            height = 150,
                            bg = '#212121')
        self.topFrame.pack(fill = X)

        # Bottom Frame
        self.bottomFrame = Frame(self,
                                height = 600,
                                bg = '#252526')
        self.bottomFrame.pack(fill = X)

        # Heading, image 
        self.top_image = PhotoImage(file = 'img/addperson.png')
        self.top_image_lbl = Label(self.topFrame,
                                image = self.top_image,
                                bg = '#212121')
        self.top_image_lbl.place(x = 120, y = 10)
        heading = Label(self.topFrame,
                        text = 'Add member',
                        font = 'Helvetica',
                        fg = '#306998',
                        bg = '#212121')
        heading.place(x = 290, y = 60)


        ### Entries and labels ###

        # Name
        self.lbl_name = Label(self.bottomFrame,
                            text = 'Name : ',
                            font = 'Helvetica',
                            fg = 'white',
                            bg = '#252526')
        self.lbl_name.place(x = 40, y = 40)

        self.ent_name = Entry(self.bottomFrame,
                            width = 30,
                            bd = 0)
        self.ent_name.insert(0, 'Please enter your name')
        self.ent_name.place(x = 150, y = 45)

        # Surname
        self.lbl_surname = Label(self.bottomFrame,
                            text = 'Surname : ',
                            font = 'Helvetica',
                            fg = 'white',
                            bg = '#252526')
        self.lbl_surname.place(x = 40, y = 80)

        self.ent_surname = Entry(self.bottomFrame,
                            width = 30,
                            bd = 0)
        self.ent_surname.insert(0, 'Please enter your surname')
        self.ent_surname.place(x = 150, y = 85)

        # Email
        self.lbl_email = Label(self.bottomFrame,
                            text = 'Email : ',
                            font = 'Helvetica',
                            fg = 'white',
                            bg = '#252526')
        self.lbl_email.place(x = 40, y = 120)

        self.ent_email = Entry(self.bottomFrame,
                            width = 30,
                            bd = 0)
        self.ent_email.insert(0,'Enter your email')
        self.ent_email.place(x = 150, y = 125)

        # Language
        self.lbl_phone = Label(self.bottomFrame,
                            text = 'Phone : ',
                            font = 'Helvetica',
                            fg = 'white',
                            bg = '#252526')
        self.lbl_phone.place(x = 40, y = 160)

        self.ent_phone = Entry(self.bottomFrame,
                            width = 30,
                            bd = 0)
        self.ent_phone.insert(0,'Please enter phone number')
        self.ent_phone.place(x = 150, y = 165)


        # Button
        btn = Button(self.bottomFrame,
                    text = 'Add Book',
                    command = self.addMember)
        btn.place(x = 270, y = 200)

    
    def addMember(self):
        name = self.ent_name.get()
        surname = self.ent_surname.get()
        email = self.ent_email.get()
        phone = self.ent_phone.get()

        if (name and surname and email and phone != ''):
            try:
                query = "INSERT INTO 'member' (member_name, member_surname, member_email, member_phone) VALUES (?, ?, ?, ?)"
                cur.execute(query, (name, surname, email, phone))
                con.commit()
                messagebox.showinfo('Success', 'Congratulations, the data is added to the database!', icon = 'info')

            except:
                messagebox.showerror('Error', 'Fucking Error, I have no clue what happen!', icon = 'error')
        else:
            messagebox.showerror('Error', 'Feilds can not be empty!', icon = 'warning')