from tkinter import *
from tkinter import ttk
import tkinter as tk
import sqlite3
from tkinter import messagebox
import addbook, addmember, givebook


con = sqlite3.connect('library.db')
cur = con.cursor()


class Main(object):
    
    def __init__(self, master):
        self.master = master

        def displayStatistics(evt):
            
            count_books = cur.execute("SELECT count(book_id) FROM books").fetchall()
            count_members = cur.execute("SELECT count(member_id) FROM member").fetchall()
            taken_books = cur.execute("SELECT count(book_status) FROM books WHERE book_status = 1").fetchall()
            #print(count_books)
            self.lbl_book_count.config(text = 'Total Books :' + str(count_books[0][0]) + ' book(s) in library')
            self.lbl_member_count.config(text = 'Total Members :' + str(count_members[0][0]))
            self.lbl_taken_count.config(text = 'Borrowed Books: ' + str(taken_books[0][0]))
            displayBooks(self)

        def displayBooks(self):
            books = cur.execute("SELECT * FROM books").fetchall()
            count = 0

            self.list_books.delete(0, END)
            for book in books:
                #print(book)
                self.list_books.insert(count, str(book[0]) + ' - ' + book[1])
                count += 1

            def bookInfo(evt):
                value=str(self.list_books.get(self.list_books.curselection()))
                id=value.split('-')[0]
                book =cur.execute("SELECT * FROM books WHERE book_id=?",(id,))
                book_info=book.fetchall()
                print(book_info)
                self.list_details.delete(0,'end')
                self.list_details.insert(0,"Book Name : "+book_info[0][1])
                self.list_details.insert(1,"Author : "+book_info[0][2])
                self.list_details.insert(2,"Page : "+book_info[0][3])
                self.list_details.insert(3,"Language : "+book_info[0][4])
                if book_info[0][5] == 0:
                    self.list_details.insert(4,"Status : Avaiable")
                else:
                    self.list_details.insert(4,"Status : Not Avaiable")

            def doubleClick(evt):
                global given_id
                value=str(self.list_books.get(self.list_books.curselection()))
                given_id=value.split('-')[0]
                give_book=GiveBook()


            self.list_books.bind('<<ListboxSelect>>', bookInfo)
            self.tabs.bind('<<NotebookTabChanged>>', displayStatistics)
            #self.tabs.bind('<ButtonRelease-1>'), displayBooks)
            self.list_books.bind('<Double-Button-1>', doubleClick)

        ### Frames ###

        mainFrame = Frame(self.master)
        mainFrame.pack()

        # Top Frame
        topFrame = Frame(mainFrame, 
                        width = 1350, 
                        height = 70, 
                        bg = '#212121', 
                        padx = 20,
                        borderwidth = 0,
                        pady = 10)
        topFrame.pack(side = TOP, fill= X)

        # Center Frame
        centerFrame = Frame(mainFrame,
                            width = 1350,
                            bg = '#252526',
                            height = 680)
        centerFrame.pack(side = TOP)

        # Center Left Frame - Child
        centerLeftFrame = Frame(centerFrame,
                                width = 900,
                                height = 700,
                                bg = '#121212',
                                borderwidth = 0)
        centerLeftFrame.pack(side = LEFT)

        # Center Right Frame - Child
        centerRightFrame = Frame(centerFrame,
                                width = 450,
                                height = 700,
                                bg = '#252526',
                                borderwidth = 0,
                                padx = 10,
                                pady = 10)
        centerRightFrame.pack()


        ### Right Side ###


        # Search Bar
        searchBar = LabelFrame(centerRightFrame,
                                width = 400,
                                height = 75,
                                text = 'Search Box',
                                bg = '#252526',
                                bd = 0,
                                fg = '#FFF')
        searchBar.pack(fill = BOTH)
        self.lbl_search = Label(searchBar,
                                text = 'Search: ',
                                font = 'Helvetica',
                                bg = '#252526',
                                fg = '#FFF',
                                pady = 10)
        self.lbl_search.grid(row = 0, column = 0)
        self.ent_search = Entry(searchBar,
                                width = 30,
                                bd = 0,
                                borderwidth = 6,
                                relief=tk.FLAT)
        self.ent_search.grid(row = 0, column = 1, columnspan = 3, padx = 10, pady = 10)
        self.ent_search.focus()
        self.btn_search = Button(searchBar,
                                text = 'Search',
                                font = 'Helvetica 9',
                                bg = '#306998',
                                fg = '#FFF',
                                bd = 0,
                                padx = 5,
                                pady = 5,
                                command = self.searchBooks)
        self.btn_search.grid(row = 0, column = 4)

        # ListBar
        listBar = LabelFrame(centerRightFrame,
                            width = 440,
                            height = 175,
                            text = 'List Box',
                            bg = '#252526',
                            bd = 0,
                            fg = '#FFF')
        listBar.pack(fill = BOTH)
        self.lbl_list = Label(listBar,
                            text = 'Sort by',
                            font = 'Helvetica',
                            fg = '#FFF',
                            bg = '#252526')
        self.lbl_list.grid(row = 0, column = 2)


        self.listChoice = IntVar()
        rb1 = Radiobutton(listBar,
                        text = 'All books',
                        var = self.listChoice,
                        value = 1,
                        bg = '#252526',
                        fg = '#FFF',
                        activebackground = '#252526')
        rb2 = Radiobutton(listBar,
                        text = 'In Library',
                        var = self.listChoice,
                        value = 2,
                        bg = '#252526',
                        fg = '#FFF',
                        activebackground = '#252526')
        rb3 = Radiobutton(listBar,
                        text = 'Borrowed Books',
                        var = self.listChoice,
                        value = 3,
                        bg = '#252526',
                        fg = '#FFF',
                        activebackground = '#252526')
        rb1.grid(row = 1, column = 0)
        rb2.grid(row = 1, column = 1)
        rb3.grid(row = 1, column = 2)
        # Button for List Box - Sort By
        btn_list = Button(listBar,
                        text = 'List Books',
                        font = 'Helvetica 9',
                        bg = '#306998',
                        fg = '#FFF',
                        bd = 0,
                        padx = 5,
                        pady = 5,
                        command = self.listBooks)
        btn_list.grid(row = 1, column = 3, padx = 40, pady = 10)
        

        ### BUTTONS - Header ###
        
        # Add Buttons
        self.iconbook = PhotoImage(file = 'img/addbook.png')
        self.btnbook = Button(topFrame,
                            image = self.iconbook,
                            font = 'Helvetica',
                            bd = 0,
                            bg = '#212121',
                            activebackground = '#212121',
                            command = self.addBook)
        self.btnbook.pack(side = LEFT)


        # Add Member Button
        self.iconmember = PhotoImage(file = 'img/add_member.png')
        self.btnmember = Button(topFrame,
                            image = self.iconmember,
                            font = 'Helvetica',
                            bd = 0,
                            bg = '#212121',
                            activebackground = '#212121',
                            command = self.addMember)
        self.btnmember.pack(side = LEFT)

        # Give book
        self.icongive = PhotoImage(file = 'img/givebook.png')
        self.btngive = Button(topFrame,
                            image = self.icongive,
                            font = 'Helvetica',
                            bd = 0,
                            bg = '#212121',
                            activebackground = '#212121',
                            command = self.giveBook)
        self.btngive.pack(side = LEFT)

        ### TABS ###

        # Tab1
        self.tabs = ttk.Notebook(centerLeftFrame,
                                width = 900,
                                height = 660)
        self.tabs.pack()
        self.tab1_icon = PhotoImage(file = 'img/books.png')
        self.tab2_icon = PhotoImage(file = 'img/members.png')
        self.tab1 = Frame(self.tabs)
        self.tab2 = Frame(self.tabs)
        self.tabs.add(self.tab1,
                    text = 'Library Management',
                    image = self.tab1_icon,
                    compound = LEFT)
        self.tabs.add(self.tab2,
                    text = 'Statistics',
                    image = self.tab2_icon,
                    compound = LEFT)

        # LIST BOOKS
        self.list_books = Listbox(self.tab1, 
                                width = 40, 
                                height = 30,
                                bd = 0,
                                font = 'Helvetica')
        self.sb=Scrollbar(self.tab1,
                        orient = VERTICAL,)
        self.list_books.grid(row = 0,
                            column = 0,
                            padx = (10, 0),
                            pady = 10,
                            sticky = N)
        self.sb.config(command = self.list_books.yview)
        self.list_books.config(yscrollcommand = self.sb.set)
        self.sb.grid(row = 0,
                    column = 0,
                    sticky = N + S + E)
        
        # List Details
        self.list_details = Listbox(self.tab1,
                                    width = 80,
                                    height = 30,
                                    bd = 0,
                                    font = 'Helvetica')
        self.list_details.grid(row = 0,
                            column = 1,
                            padx = (10, 0),
                            pady = 10,
                            sticky = N)
        

        ### Tab2 ###

        # Statistic
        self.lbl_book_count = Label(self.tab2,
                                    text = '',
                                    pady = 20,
                                    font = 'Helvetica')
        self.lbl_book_count.grid(row = 0)
        self.lbl_member_count = Label(self.tab2,
                                    text = '',
                                    pady = 20,
                                    font = 'Helvetica')
        self.lbl_member_count.grid(row = 1, sticky = W)
        self.lbl_taken_count = Label(self.tab2,
                                    text = '',
                                    pady = 20,
                                    font = 'Helvetica')                     
        self.lbl_taken_count.grid(row = 2, sticky = W)



        #functions
        displayBooks(self)
        displayStatistics(self)

    def addBook(self):
        # addbook.py is the file
        add=addbook.AddBook()

    def addMember(self):
        member = addmember.AddMember()

    def searchBooks(self):
        value = self.ent_search.get()
        search = cur.execute("SELECT * FROM books WHERE book_name LIKE ?", ('%' + value + '%', )).fetchall()
        print(search)
        self.list_books.delete(0, 'end')
        count = 0
        for book in search:
            self.list_books.insert(count, str(book[0]) + ' - ' + book[1])
            count += 1
    
    def listBooks(self):
        value = self.listChoice.get()
        if value == 1:
            allbooks = cur.execute("SELECT * FROM books").fetchall()
            self.list_books.delete(0, END)

            count = 0
            for book in allbooks:
                self.list_books.insert(count, str(book[0]) + ' - ' + book[1])
                count += 1
        
        elif value == 2:
            books_in_library = cur.execute("SELECT * FROM books WHERE book_status = ?", (0,)).fetchall()
            self.list_books.delete(0, END)

            count = 0
            for book in books_in_library:
                self.list_books.insert(count, str(book[0]) + ' - ' + book[1])
                count += 1
        else:
            taken_book = cur.execute("SELECT * FROM books WHERE book_status = ?", (1,)).fetchall()
            self.list_books.delete(0, END)

            count = 0
            for book in taken_book:
                self.list_books.insert(count, str(book[0]) + ' - ' + book[1])
                count += 1
        
    def giveBook(self):
        give_book = givebook.GiveBook()

class GiveBook(Toplevel):
    def __init__(self):
        Toplevel.__init__(self)
        self.geometry('650x750+550+200')
        self.title('Borrowed Books')
        self.resizable(False, False)
        global given_id
        self.book_id = int(given_id)
        query = "SELECT * FROM books"
        books = cur.execute(query).fetchall()
        book_list = []
        for book in books:
            book_list.append(str(book[0])+ '-' + book[1])

        query2 = "SELECT * FROM member"
        members = cur.execute(query2).fetchall()
        member_list = []
        for member in members:
            member_list.append(str(member[0])+ '-' + member[1])

        ### Frames ###

        # Top Frame
        self.topFrame = Frame(self,
                            height = 150,
                            bg = '#212121')
        self.topFrame.pack(fill = X)

        # Bottom Frame
        self.bottomFrame = Frame(self,
                                height = 600,
                                bg = '#252526')
        self.bottomFrame.pack(fill = X)

        # Heading, image 
        self.top_image = PhotoImage(file = 'img/addperson.png')
        self.top_image_lbl = Label(self.topFrame,
                                image = self.top_image,
                                bg = 'white')
        self.top_image_lbl.place(x = 120, y = 10)
        heading = Label(self.topFrame,
                        text = 'Lend to',
                        font = 'Helvetica',
                        fg = '#FFF',
                        bg = '#212121')
        heading.place(x = 290, y = 60)


        ### Borrowed Window - Entries and labels ###

        # Borrowed Book
        self.book_name = StringVar()
        self.lbl_name = Label(self.bottomFrame,
                            text = 'Book Name ',
                            font = 'Helvetica',
                            fg = 'white',
                            bg = '#252526')
        self.lbl_name.place(x = 40, y = 40)
        self.combo_name = ttk.Combobox(self.bottomFrame,
                                    textvariable = self.book_name)
        self.combo_name['values'] = book_list
        self.combo_name.current(self.book_id - 1)
        self.combo_name.place(x = 150, y = 45)


        # Member that borrows - Name
        self.member_name = StringVar()
        self.lbl_surname = Label(self.bottomFrame,
                            text = 'Member Name : ',
                            font = 'Helvetica',
                            fg = 'white',
                            bg = '#212121')
        self.lbl_surname.place(x = 40, y = 80)
        self.combo_member = ttk.Combobox(self.bottomFrame,
        text = '124',
                                    textvariable = self.member_name)
        self.combo_member['values'] = member_list
        self.combo_member.place(x = 150, y = 85)

        # Button
        btn = Button(self.bottomFrame,
                    text = 'Lend Book',
                    command = self.lendBook)
        btn.place(x = 220, y = 150)

    def lendBook(self):
        book_name = self.book_name.get()
        member_name = self.member_name.get()


        if(book_name and member_name != ''):
            try:
                query = "INSERT INTO 'borrows' (bbook_id, bmember_id) VALUES (?, ?)"
                cur.execute(query,(book_name, member_name))
                con.commit()
                messagebox.showinfo("Success", "Successfully added to the database!", icon = 'info')
                cur.execute("UPDATE books SET book_status = ? WHERE book_id = ?", (1, self.book_id))
                con.commit()
            except:
                messagebox.showerror('Error', 'Can NOT be added to the database!', icon = 'warning')  
        else:
            messagebox.showerror('Error', 'Fields can NOT be empty!', icon = 'warning')        


def main():
    root = Tk()
    app = Main(root)
    root.title('Library Managment System')
    root.geometry('1350x750+350+200')
    root.iconbitmap('img/icon.ico')
    root.resizable(False, False)
    root.mainloop()

if __name__ == '__main__':
    main()
